#Identify pupils. Based on beta 1

import numpy as np
import cv2
import time
cap = cv2.VideoCapture(0) 	#640,480
w = 640
h = 480

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
	
		#downsample
		#frameD = cv2.pyrDown(cv2.pyrDown(frame))
		#frameDBW = cv2.cvtColor(frameD,cv2.COLOR_RGB2GRAY)
	
		#detect face
		# frame = cv2.cvtColor(frame,cv2.COLOR_RGB2GRAY)
		frame = cv2.cvtColor(frame,cv2.COLOR_RGB2GRAY)
		faces = cv2.CascadeClassifier('haar3right.xml')
		frame = cv2.equalizeHist(frame,frame)
		detected = faces.detectMultiScale(frame, 1.3, 5)
		
	
		#faces = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
		#detected2 = faces.detectMultiScale(frameDBW, 1.3, 5)
		
		pupilFrame = frame
		pupilO = frame
		windowClose = np.ones((5,5),np.uint8)
		windowOpen = np.ones((2,2),np.uint8)
		windowErode = np.ones((2,2),np.uint8)


		#draw square
		for (x,y,w,h) in detected:
            
			cv2.rectangle(frame, (x,y), ((x+w),(y+h)), (0,0,255),1)	
			cv2.line(frame, (x,y), ((x+w,y+h)), (255,255,255),1)
			cv2.line(frame, (x+w,y), ((x,y+h)), (255,255,255),1)

		cv2.imshow('frame2',frame)

		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

# Release everything if job is finished
cap.release()
cv2.destroyAllWindows()